# /usr/bin/python3
####### PACKAGES

import numpy as np
import numpy.linalg as npl

# METHODS TO ANALYZE

# Aster Methods

def distance(frame):   
    return np.linalg.norm(frame.objects["solid"]["core"][0].position)

def position(frame):   
    return frame.objects["solid"]["core"][0].position

def position_x(frame):   
    return frame.objects["solid"]["core"][0].position[0]


def colin(frame):   
    pos = frame.objects["solid"]["core"][0].position
    force = frame.objects["solid"]["core"][0].force
    return np.dot(pos,force)/(npl.norm(pos)*npl.norm(force))


def total_force(frame): 
    return frame.objects["solid"]["core"][0].force

def total_force_x(frame): 
    return frame.objects["solid"]["core"][0].force


def total_individual_forces(frame):
    return np.sum(frame.objects["solid"]["core"][0].fiber_forces[:,3:5],axis=0)

def total_individual_forces_x(frame):
    return np.sum(frame.objects["solid"]["core"][0].fiber_forces[:,3:5],axis=0)[0]

# Tweezer Methods

def tweezer_distance(frame):
    return np.linalg.norm(frame.objects["space"]["tweezer"][0].position)

def tweezer_X_distance(frame):
    return np.linalg.norm(frame.objects["space"]["tweezer"][0].position)

# CYTOSIM ANALYSIS
