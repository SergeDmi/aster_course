% A confined aster
% F. Nedelec, April 2010 - Sep 2017

set simul system
{
    time_step = 0.02
    viscosity = 0.01
}

set space cell
{
    shape = sphere
}

new cell
{
    radius = 5
}

set fiber microtubule
{
    rigidity = 20
    segmentation = 1
%    confine = inside, 100
    activity = classic


    growing_speed    = 2
    shrinking_speed  = -3
    growing_force    = 1.67
    catastrophe_rate  = 0.01
    catastrophe_rate_stalled = 0.05
    rescue_rate      = 1
    min_length       = 0.5
    max_length       = 10

    persistent       = 1
 
    display = ( line_width = 4; )
}

set solid core
{
    display = ( style = 3 )
}

set aster centrosome
{
    stiffness = 1000, 500
    nucleate = 1, microtubule, ( plus_end=grow; length=10; )
}

new centrosome
{
    solid = core
    radius = 0.25
    point1 = center, 0.25
    fibers = 200, microtubule, ( plus_end=grow; length = 1; )
    position = 3,0
}
 
set hand dynein
{
    binding_rate = 5
    binding_range = 0.02
    unbinding_rate = 2
    unbinding_force = 3
    
    activity = move
    unloaded_speed = -1
    stall_force = 5
    display = ( color = green; dark_gray; )
}

set single grafted
{
    hand = dynein
    stiffness = 100
    activity = diffuse
}

set bead ball
{
    steric = 1
    confine = inside, 100
}

new 1000 bead ball
{
    radius = .02
    attach = grafted
%    position = edge 0.01
}

run system
{
    nb_steps = 10000
    nb_frames = 200
}
