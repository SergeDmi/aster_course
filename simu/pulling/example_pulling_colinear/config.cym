% A confined aster
% F. Nedelec, April 2010

set simul system
{
    time_step = 0.01
    viscosity = 0.01
}

set space cell
{
    shape = sphere
}

new cell
{
    radius = 10
}

set fiber microtubule
{
    rigidity = 20
    segmentation = 0.5
    confine = inside, 100
    activity = classic

    growing_speed            = 1
    shrinking_speed          = -3
    catastrophe_rate         = 0.1
    catastrophe_rate_stalled = 0.1
    rescue_rate              = 1
    growing_force            = 0.5
    min_length 				 = 0.5
    max_length		         = 20

    persistent = 1
    % microtubule is pulling around
    colinear_force           = 0.05, 1.0
    display = { line_width=5; }
}

set solid core
{
    display = ( style=3; color=blue; )
    confine = inside, 100, cell
}

set aster centrosome
{
    stiffness = 1000, 500
}

new centrosome
{
    solid = core
    radius = 0.5
    point1 = center, 0.5
    seed_diameter = 0.02
    fibers = 100, microtubule, ( length = 2; plus_end = green;)
    position = -6,0
}


run system
{
    nb_steps = 10000
    nb_frames = 100
}
