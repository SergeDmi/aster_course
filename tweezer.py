# /usr/bin/python3
####### PACKAGES

from cytolysis.objects import Object_set,Object
import numpy as np


class Tweezer_set(Object_set):

    """ Aster_set is a set of aster
        Aster is a solid, of which the position, force, and linker forces may be reported
        """

    def read_objects(self, *args, reports=None, **kwargs):
        DIM = self.dim

        if reports is not None:
            keys = reports.keys()
            if 'position' in keys:
                #this is too long yes...
                f =reports['position']

                lines = [line.split() for line in f if (line[0] != '%')]
                lines = [line for line in lines if (len(line)!= 0)]
                lines = [line for line in lines if line[0] != 'tweezer']
                lines = np.resize(lines,(len(lines)//3,3))

                for row in lines:
                    self.append(Object(position=row[0]))

